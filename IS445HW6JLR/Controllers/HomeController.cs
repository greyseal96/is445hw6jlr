﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IS445HW6JLR.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.PageTitle = "Home Page";
            ViewBag.TitleMessage = "IS445HW6";
            ViewBag.Message = "Click on a task link to see some string manipulation at work in both C# and Javascript.";

            return View();
        }
    }
}
