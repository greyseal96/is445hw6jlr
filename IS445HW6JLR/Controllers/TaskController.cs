﻿using IS445HW6JLR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IS445HW6JLR.Controllers
{
    public class TaskController : Controller
    {
        //
        // GET: /Task/Task1

        public ActionResult Task1()
        {
            ViewBag.PageTitle = "Task 1";
            ViewBag.TitleMessage = "Task 1";
            ViewBag.Message = "Format a phone number into a standard format using C#.";

            return View();
        }

        //
        // GET: /Task/Task1Format
        [HttpGet]
        public ActionResult Task1Format()
        {
            ViewBag.PageTitle = "Task 1";
            ViewBag.TitleMessage = "Task 1";
            ViewBag.Message = "Format a phone number into a standard format using C#.";
            PhoneNumberModel phoneNumModel = new PhoneNumberModel();

            return View(phoneNumModel);
        }

        //
        // POST: /Task/Task1Format
        [HttpPost]
        public ActionResult Task1Format(PhoneNumberModel phoneNumModel)
        {
            string regexPattern = @"[0-9]+";
            string inputString = phoneNumModel.PhoneNumber;
            StringBuilder outputSB = new StringBuilder();
            MatchCollection regexMatchColl = Regex.Matches(inputString, regexPattern);

            foreach (Match regexMatch in regexMatchColl)
            {
                outputSB.Append(regexMatch.Value);
            }

            if (outputSB.Length != 10)
            {
                //We've got a problem.  Return to the user and let them know that they didn't submit
                //a valid 10 digit phone number
                ModelState.AddModelError("PhoneNumber", "The phone number must have 10 digits");

                ViewBag.PageTitle = "Task 1";
                ViewBag.TitleMessage = "Task 1";
                ViewBag.Message = "Format a phone number into a standard format using C#.";

                return View(phoneNumModel);
            }

            else
            {
                //Start with:
                //xxxxxxxxxx
                //0123456789  <--Index
                outputSB.Insert(0, "(");

                //Now we have:
                //(xxxxxxxxxx
                //01234567890
                outputSB.Insert(4, ") ");

                //Now we have:
                //(xxx) xxxxxxx
                //0123456789012
                outputSB.Insert(9, "-");

                //Finally, it is formatted correctly.
                //(xxx) xxx-xxxx
                PhoneNumberModel newPhoneNumModel = new PhoneNumberModel();
                newPhoneNumModel.PhoneNumber = outputSB.ToString();

                ViewBag.PageTitle = "Task 1";
                ViewBag.TitleMessage = "Task 1";
                ViewBag.Message = "Format a phone number into a standard format using C#.";

                return View("Task1FormatResult", newPhoneNumModel);
            }
        }

        //
        // GET: /Task/Task2
        public ActionResult Task2()
        {
            ViewBag.PageTitle = "Task 2";
            ViewBag.TitleMessage = "Task 2";
            ViewBag.Message = "Reverse the text of a provided sentence using Javascript.";

            return View();
        }

        public ActionResult Task2Format()
        {
            ViewBag.PageTitle = "Task 2";
            ViewBag.TitleMessage = "Task 2";
            ViewBag.Message = "Reverse the text of a provided sentence using Javascript.";

            return View();
        }
    }
}
