﻿using System.Web;
using System.Web.Mvc;

namespace IS445HW6JLR
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}