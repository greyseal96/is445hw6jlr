﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IS445HW6JLR.Models
{
    public class PhoneNumberModel
    {
        [DisplayName("Phone Number")]
        [Required]
        public string PhoneNumber { get; set; }
    }
}